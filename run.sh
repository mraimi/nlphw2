#!/bin/bash
# python rare_replace.py
# python count_cfg_freq.py parse_train_rare.dat > cfg.counts
# python CKY.py > prediction_file
# python eval_parser.py parse_dev.key prediction_file > Q5results

python rare_replace2.py
python count_cfg_freq.py parse_train_vert_rare.dat > cfg.counts
python CKY.py > prediction_file
python eval_parser.py parse_dev.key prediction_file > Q6results
