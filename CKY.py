#! /usr/bin/python

import sys,json,math
from collections import defaultdict

corpusfile = open('cfg.counts','r')

class Hmm:
	def __init__(self):
  		self.emissions = [dict() for i in xrange(2)]
  		self.rule_counts = [dict() for i in xrange(4)]
  		self.words = set()
  		self.agg_rules = dict()

	def read_counts(self, corpusfile):
		for line in corpusfile:
			parts = line.strip().split(" ")
			count = float(parts[0])
			if parts[1] == "NONTERMINAL":
				POS = parts[2]
				self.rule_counts[0][(POS)] = count
			elif parts[1] == "BINARYRULE":
				parent = parts[2]
				child1 = parts[3]
				child2 = parts[4]
				self.rule_counts[1][(parent, child1, child2)] = count
			elif parts[1] == "UNARYRULE":
				POS = parts[2]
				word = parts[3]
				self.words.add(word)
				self.rule_counts[2][(POS, word)] = count
		for (parent, child1, child2) in self.rule_counts[1].iterkeys():
			if parent not in self.agg_rules:
				self.agg_rules[(parent)] = [(parent, child1, child2)]
			else:
				self.agg_rules[(parent)].append((parent,child1,child2))
	
	def get_emiss(self):
		#binary case
		for (parent, child1, child2), count in self.rule_counts[1].iteritems():
			if (parent, child1, child2) not in self.emissions[0]:
				self.emissions[0][(parent, child1, child2)] = math.log(float(count/self.rule_counts[0][(parent)]))
			else:
				self.emissions[0][(parent, child1, child2)] += math.log(float(count/self.rule_counts[0][(parent)]))
		#unary case
		for (POS, word), count in self.rule_counts[2].iteritems():
			if POS not in self.emissions[1]:
				self.emissions[1][(POS, word)] = math.log(float(count/self.rule_counts[0][(POS)]))
			else:
				self.emissions[1][(POS, word)] += math.log(float(count/self.rule_counts[0][(POS)]))

	def print_emiss(self):
		for i in xrange(2):
			if i == 0:
				for (p, c1, c2), pr in self.emissions[i].iteritems():
					print p + " " + c1 + " " + c2 + " : " + str(pr)
			if i == 1:
				for (p, w), pr in self.emissions[i].iteritems():
					print p + " " + w + " : " + str(pr)
	
	def CKY(self, corpusfile):
		source_file = open(corpusfile, 'r')
		dest_file = open('prediction_file', 'w')
		for line in source_file:
			pi = dict()
			bp = dict()
			ln = line.strip().split(" ")
			length = len(ln)

			for i in xrange(0,length):
				for rule in self.rule_counts[0].iterkeys():
					if ln[i] in self.words:	
						if (rule, ln[i]) in self.rule_counts[2]:
							pi[(i,i,rule)] = self.emissions[(1)][(rule,ln[i])]
						else:
							pi[(i,i,rule)] = float('-inf')
					else:
						if (rule, "_RARE_") in self.rule_counts[2]:
							pi[(i,i,rule)] = self.emissions[(1)][(rule,"_RARE_")]
						else:
							pi[(i,i,rule)] = float('-inf')


			for l in xrange(1,length):
				for i in xrange(0,length-l):
					j = i+l
					for ex in self.rule_counts[0].iterkeys():
						max_rule = None
						max_prob = float('-inf')
						max_split = 0
						if ex not in self.agg_rules:
							pi[(i,j,ex)] = float('-inf')
							continue
						for (X, Y, Z) in self.agg_rules[(ex)]:
							if max_rule == None:
								max_rule = (X,Y,Z)
							# if ex != X: #skip binary rules not matching left side
							# 	break
							for s in xrange(i,j): #xrange would terminate early at j-1
								p = self.emissions[0][(X,Y,Z)]
								a = pi[(i,s,Y)]
								b = pi[(s+1,j,Z)]
								p = p+a+b
								if  p > max_prob:
									max_rule = (X,Y,Z)
									max_prob = p
									max_split = s
						pi[(i,j,ex)] = max_prob
						bp[(i,j,ex)] = (max_rule,max_split)

			# for i in xrange(0,length):
			# 	for j in xrange(i,length):
			# 		for rule in self.rule_counts[0].iterkeys():
			# 			print rule + " " +str(i) +" "+ str(j) +" "+str(pi[(i,j,rule)])
			if  math.isinf(pi[(0,length-1,"S")]):
				max_prob = float('-inf')
				max_X = None
				for X in self.rule_counts[0]:
					if max_X == None:
						max_X = X
					if pi[(0,length-1,X)] > max_prob:
						max_prob = pi[(0,length-1,X)]
						max_X = X
				tree = self.get_tree(bp,0,length-1,max_X,ln)
				# print tree
				new_tree = json.dumps(tree)
				dest_file.write(new_tree + "\n")
			else:
				tree = self.get_tree(bp,0,length-1,"S",ln)
				new_tree = json.dumps(tree)
				dest_file.write(new_tree + "\n")

	def get_tree(self,bp,start,end,root,ln):
		if start == end:
			return [root, ln[start]]
		else:
			(rule,split)=bp[start,end,root]
			return[root, self.get_tree(bp,start,split,rule[1],ln), self.get_tree(bp,split+1,end,rule[2],ln)]

if __name__ == "__main__":
	counter = Hmm()
	counter.read_counts(corpusfile)
	counter.get_emiss()
	counter.CKY('parse_dev.dat')
