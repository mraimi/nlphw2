#! /usr/bin/python

import sys,json
from count_cfg_freq import Counts

counter = Counts()
for l in open('parse_train.dat'):
    t = json.loads(l)
    counter.count(t)
# counter.show()
simple_counts = dict()
rare_words = set()
for (sym, word), count in counter.unary.iteritems():
	if word not in simple_counts:
		simple_counts[(word)] = count
	else:
		simple_counts[(word)] += count

for word, count in simple_counts.iteritems():
	if count < 5:
		rare_words.add(word)

def print_set(s):
	for word in rare_words:
		print word

def print_dict(d):
	for word, value in d.iteritems():
		print word + ": " + str(value)

def replace(tree):
    if isinstance(tree, basestring): return
    
    if len(tree) == 3:
      replace(tree[1])
      replace(tree[2])
    elif len(tree) == 2:
      if tree[1] in rare_words:
      	tree[1] = "_RARE_"

ptr = open('parse_train_vert_rare.dat', 'w')

for l in open('parse_train_vert.dat'):
	t = json.loads(l)
	replace(t)
	new_tree = json.dumps(t)
	ptr.write(new_tree + "\n")

ptr.close()

# print_dict(simple_counts)
